# Edwin Romero - Coding Interview

I've read the [ReadMe.md](https://gitlab.com/beontechstudio/data-engineer-coding-interview/-/blob/main/README.md?ref_type=heads&plain=1) file and the intention of this challenge is to asses the developer's ability to extract from a source of data and make it available for analysis.

For this challenge, I've decided to use Databricks as an execution platform because it fulfills 3 out of the 3 requirements established for this challange, which are:

1. Visual ETL orchestration  --- _instead of dbt_
2. Load dataset from CSV and save them in a Delta tables 
3. SQL interface through Spark & Delta tables for analytics -- _instead of postgres_ 


### Visual ETL orchestration
In databricks we can create workflows that execute notbeooks in which Spark is being used to extract the CSV files and load them into a dataframe, to finally be saved as a Delta table for the analytics team to use.
I'll create a parametrized notebook for each step with the intention to engage a graphic ETL tool interface.

Steps:
1. Load a Dataset for a configuration file & create its corresponding delta table.
2. Execute basic data quality process to ensure data accuracy.
3. Analytics team could create a predifined set of aggregations to generate plots.  -- Used in daily generated files.

### Load dataset from CSV & create delta table
The source CSV file could be stored in different platforms and locations. For the purpose of interacting with cloud services, I've uploaded the CSV files into an S3 bucket and then load it into an Spark session. 

To upload all the CSV files to s3 with a single command:
```bash
> aws s3 $CSV_FOLDER $S3_TARGET_PATH --recursive
``` 

### SQL interface
This is accessible through the SQL Editor service in Databricks.