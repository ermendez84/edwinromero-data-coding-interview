# Databricks notebook source
# MAGIC %run ./CSV

# COMMAND ----------

from datetime import datetime as dt
from pyspark.sql.functions import lit
from pytz import timezone as tz

# Collecting execution parameters
env = dbutils.widgets.get("env")
ds_name = dbutils.widgets.get("dataset")
raw_tbl_name = f"audit_{env}_db.raw_{ds_name}"

# Not the best approach but, it is a really small set of data.
# Collecting raw data
sql = f"select * from {raw_tbl_name}"
df = spark.sql(sql)

# Removing duplicates and dropping NAs 
df = df.drop_duplicates()
df = df.na.drop()
df = df.withColumn('ts_ms', lit(dt.now(tz('US/Pacific'))))

# Initiating class and writing delta table
instance = Loader(ds_name)
tbl_name = instance.write(df=df, layer='silver')

# Optimizing delta table for analytics
spark.sql(f"optimize {tbl_name}")
