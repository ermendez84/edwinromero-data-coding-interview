# Databricks notebook source
# MAGIC %run ./CSV

# COMMAND ----------

from datetime import datetime as dt
from pytz import timezone as tz
from pyspark.sql.functions import lit

# Reading CSV file
ds = dbutils.widgets.get('dataset')
ds = Loader(ds)
df = ds.read()

# Once the CSV has been read, a load registry must be implemented.
df = df.withColumn('ts_ms', lit(dt.now(tz('US/Pacific'))))

# Creating the raw delta table based on the data ingested from then CSV
ds.write(df=df, layer='raw')

# Validate data insertion
tbl_name = f"audit_{ds.env}_db.raw_{ds.ds_name}"
spark.sql(f'select * from {tbl_name}').show()
