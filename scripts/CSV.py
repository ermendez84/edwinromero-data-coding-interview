# Databricks notebook source
class Loader:

    def __init__(self, ds_name:str):
        # Collecting execution parameters
        self.ds_name = ds_name
        self.env = dbutils.widgets.get('env')
        self.s3 = dbutils.secrets.get(f'edwin_{self.env}', 's3_location')
        self.full_path = f"{self.s3}/nyc_{self.ds_name}.csv"

    def read(self):
        # Reading CSV file
        df = spark.read \
            .format("csv") \
            .option("mode", "PERMISSIVE") \
            .option("header", 'true') \
            .option('inferSchema', 'true') \
            .option('delimiter', ',') \
            .load(self.full_path)
        return df
    
    def write(self, layer, df):
        # Creating persistent tableq
        if layer == 'raw':
            layer = 'raw_'
        elif layer == 'silver':
            layer = ''
        tbl_name = f"audit_{self.env}_db.{layer}{self.ds_name}"
        df.write. \
            mode("overwrite"). \
            option("overwriteSchema", "true"). \
            format("delta"). \
            saveAsTable(f"{tbl_name}")
        return tbl_name


